//@flow
import React from "react";
import { Input } from "./index";
import { SearchIcon } from "./icons";

type FieldProps = {
	type: string;
	name: string;
	value: number;
	handleChange: () => void;
}

/**
 * @param {string}type
 * @param {string}name
 * @param {string}value
 * @type {React$StatelessFunctionalComponent<unknown>}
 */
const Field = ({type, name, value, handleChange}: FieldProps) => {
	return type === "number" ? (
		<Input type={type} min={1} data-cy={name} name={name} value={value} onChange={handleChange}/>)
		: (
			type === "text" ?
				<Input type={type} data-cy={name} name={name} value={value} placeholder="Recherche..." onChange={handleChange}/>
				:
				type === "submit" ?
					<Input type={type} data-cy={type} value="AJOUTER AU PANIER" />
					:
					<div><SearchIcon color="currentColor"/></div>
		);
};

export default Field;


