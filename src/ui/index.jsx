//@flow
import styled from "styled-components";

import {space} from "../helpers/functions";
import type { wrapperProps } from "../services/types";


const PERCENT = "%";

export const Wrapper = styled.div`
	width: ${({wh}:wrapperProps) => space(wh) + PERCENT};
	height: ${({ht}: wrapperProps) => ht === undefined ? "auto" : ht};
	margin:  0 auto;
`;

export const ButtonCustom = styled.button`
	width: ${props => props.id};
	padding: 3px 8px;
	margin: 5px 3px;
	cursor: pointer;
	//border: none;
	border-radius: 5px;
	transition: background-color ease 3s;
	&:hover {
      background-color: #f8ebeb;
    }
`;

export const Card = styled.div`
	display: flex;
	flex-direction: ${({direction}) => direction === "row" ? "row" : "column"};
	width: ${({id}) => !id ? "340px": "100%"};
	margin: 20px 0;
	cursor: pointer;
	box-shadow: ${({shadow}) => shadow ? "rgba(0, 0, 0, 0.12) 0px 2px 10px, rgba(0, 0, 0, 0.16) 0px 2px 5px" : "none"};
`;

export const Input = styled.input`
	width: ${({type}) => type === "number" ? "10%" : (type === "radio" ? "5%" : "60%")};
	height: ${({type}) => type === "number" ? "30px" : type === "text" ? "20px" : "36px"};
	margin-right: ${({type}) => type === "number" ? "20px" : "none"};
	transition: ${({type}) => type === "submit" && "background-color ease 3s"};
	cursor: ${({type}) => type === "radio" ? "pointer" : "auto"};
	&:hover {
		background-color: ${({type}) => type === "submit" && "red"};
	}
`;

export const Badge = styled.div`
	width: 20px;
	height: 20px;
	line-height: 20px;
	border-radius: 50%;
	position: absolute;
	color: black;
	top: .7%;
	right: 2.7%;
	z-index: 10;
	font-size: .7em;
	font-weight: bold;
	background-color: white;
`;