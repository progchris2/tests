import React, { useEffect, useState } from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import BooksListScreen from "../../screens/BooksListScreen";
import useBookshop from "../../services/hooks/useBookshop";
import { LayoutApp } from "../../components";
import BookScreen from "../../screens/BookScreen";
import useBasket from "../../services/hooks/useBasket";
import BasketScreen from "../../screens/BasketScreen";
import {ArticleContext, UpdateBasketContext, FilterContext } from "../../services/context";


/**
 * global component representing,
 * the entry of the application and the different routes
 * @param props
 * @return {React.Element}
 * @constructor
 */
const Routes = (props): React.Element => {
	const [updateBasket, setUpdateBasket]  = useState(false);
	const [filter, setFilter]  = useState("");
	const {books, getAllBooks} = useBookshop();
	const {article, search, setSearch, dispatch, promotion, getPromotionArticle} = useBasket();
	//collect all the books
	useEffect(() => {
		getAllBooks();
	}, [filter]);
	
	//recover promotions
	useEffect(() => {
		if (article.article.length > 0) {
			getPromotionArticle(article.article);
		}
	}, [article]);
	
	//context global
	const valueContext = {
		search,
		article,
		promotion,
		handleDispatch  : dispatch,
		handleSearchBook: setSearch,
		getPromotionArticle
	};
	
	//context global update basket
	const updateContext = {
		updateBasket,
		handleUpdateBasket: setUpdateBasket
	};
	
	//context of the filter
	const filterContext = {
		filter,
		handleFilter: setFilter
	};
	
	return (
		<ArticleContext.Provider value={valueContext}>
			<UpdateBasketContext.Provider value={updateContext}>
				<FilterContext.Provider value={filterContext}>
					<Router>
						<Switch>
							<LayoutApp>
								<Route path="/" render={() => <BooksListScreen books={books.books} {...props}/>} exact/>
								<Route path="/books/:id" render={() => <BookScreen books={books.books}  {...props} />} exact/>
								<Route path="/basket" render={() => <BasketScreen article={article.article} {...props} />} exact/>
							</LayoutApp>
						</Switch>
					</Router>
				</FilterContext.Provider>
			</UpdateBasketContext.Provider>
		</ArticleContext.Provider>
	);
};

export default Routes;
