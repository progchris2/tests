//@flow

type createReducerType = {
	type: string;
	payload: any
}

/**
 *
 * @param initState
 * @param fnMap
 * @returns {function(*=, {type: *, payload?: *}): *}
 */
export const createReducer = (initState: any, fnMap: any) => {
	return (state: any = initState, {type, payload}: createReducerType) => {
		const handler = fnMap[type];
		return handler ? handler(state, payload) : state;
	};
};