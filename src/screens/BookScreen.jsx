//@flow
import React, { useContext, useEffect } from "react";
import { withRouter } from "react-router";
import { isEmpty } from "lodash";

import { Card } from "../ui";
import { getModelElementDOM, normalizerText } from "../helpers/functions";
import Form from "../components/form/form";
import Modal from "../components/modal/modal";
import useBook from "../services/hooks/useBook";
import type { BookScreenProps } from "../services/types";
import { ArticleContext, UpdateBasketContext } from "../services/context";


/**
 *
 * @param books
 * @param match
 * @return {JSX.Element|null}
 * @constructor
 */
const BookScreen = ({books, match}: BookScreenProps) => {
	const {book, modal, setBook, setModal} = useBook();
	const {article} = useContext(ArticleContext);
	const {handleUpdateBasket} = useContext(UpdateBasketContext);
	
	useEffect(() => {
		let bks = books;
		if (isEmpty(books)) {
			// eslint-disable-next-line no-undef
			bks = localStorage.getItem("@data-books");
			if (bks) bks = JSON.parse(bks);
		}
		
		setBook( bks && bks.filter(item => item.isbn === match.params.id)[0]);
	}, []);
	
	const handleOnClick = (): void => {
		setModal(false);
		handleUpdateBasket(false);
	};
	
	return !isEmpty(book) ? (
		<>
			{modal && <Modal articles={article.article} handleOnClick={handleOnClick} />}
			<Card className="book-detail" direction="row" shadow={true} id="true">
				<div><img src={book.cover} alt="" style={{width: "340px", height: "500px"}}/></div>
				<div>
					<div>
						<div>
							<Card direction="column">
								<div>
									<h3>{book.title}</h3>
									<div className="price">{book.price}&euro; </div>
								</div>
								<div className="form">
									<Form data={getModelElementDOM("choice-article")} book={book} handleModal={setModal}/>
								</div>
							</Card>
						</div>
						<div className="synopsis">
							{normalizerText(book.synopsis, 0, true).map((item, i) => <p key={i}>{item}</p>)}
						</div>
					</div>
				</div>
			</Card>
		</>)
		: null;
};

export default withRouter(BookScreen);
