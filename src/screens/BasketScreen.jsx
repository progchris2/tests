import React, { useEffect, useContext, useState } from "react";
import { withRouter } from "react-router";

import { CloseIcon } from "../ui/icons";
import * as types from "../services/stores/types";
import useFactoryBasket from "../services/hooks/useFactoryBasket";
import { UpdateBasketContext } from "../services/context";
import { getBestPromotion } from "../helpers/functions";
import { isEmpty } from "lodash";


const BasketScreen = () => {
	const [global, setGlobal]  = useState(0);
	const {updateBasket, handleUpdateBasket} = useContext(UpdateBasketContext);
	const {collectArticle, promotion, handleDispatch, verifyLocalStorage} = useFactoryBasket();
	
	useEffect(() => {
		verifyLocalStorage();
		handleUpdateBasket(false);
	}, [updateBasket]);
	
	const handleDeleteArticle = (payload) => {
		handleDispatch({type: types.BASKET.DELETE_ALL_ARTICLE, payload});
		handleUpdateBasket(true);
	};
	
	const decrementCount = (book) => {
		handleDispatch({type: types.BASKET.DELETE_ONE_COUNT_ARTICLE, payload: book});
		handleUpdateBasket(true);
	};
	
	const incrementCounter = (book) => {
		handleDispatch({type: types.BASKET.ADD_ONE_COUNT_ARTICLE, payload: book});
		handleUpdateBasket(true);
	};
	
	/**
	 * calculation of total items by entities
	 * @param cp
	 * @param price
	 * @return {number}
	 */
	const totalItem = (cp: number, price: number): number => cp * price;
	
	const generalTotals = (collect: Array<*>): number => {
		console.log(collect);
		let sum = 0;
		collect.forEach(item => {
			sum += totalItem(item.count,getBestPromotion(promotion, item.book.price));
		});
		return sum;
	};
	
	return (
		<div>
			{
				collectArticle.article && collectArticle.article.map((item, i) => (
					<div key={`article-${i}`}>
						<div className="basket-wrap">
							<img src={item.book.cover} alt="" style={{width: "150px", height: "190px"}}/>
							<div>
								<div className="basket-details">
									<div className="title">
										{item.book.title}
									</div>
									<div className="promotion">
										<div className="basket-price">
											<div className="price">{getBestPromotion(promotion, item.book.price)}&euro;</div>
											<div className="reduction">
												<span>promotion</span>
												<span className="price-init">{item.book.price}&euro;</span>
											</div>
											<div className="count">
												<span onClick={() => decrementCount(item)}>-</span>
												{item.count}
												<span onClick={() => incrementCounter(item)}>+</span>
											</div>
											<div className="total">{totalItem(item.count, getBestPromotion(promotion, item.book.price))}&euro;</div>
										</div>
										<div className="delete" onClick={() => handleDeleteArticle(item.book.isbn)}>
											<CloseIcon color="currentColor" /><div>supprimer</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				))
			}
			{!isEmpty(collectArticle.article) && <div className="total-global" style={{textAlign: "right"}}>
				<span>Total :</span> {generalTotals(collectArticle.article)} &euro;
			</div>}
		</div>
	);
};

export default withRouter(BasketScreen);
