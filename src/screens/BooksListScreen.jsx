//@flow
import React, { useContext } from "react";
import { withRouter } from "react-router";

import Book from "../components/books/book";
import type { bookListProps, containBook } from "../services/types";
import { ArticleContext, FilterContext } from "../services/context";
import { isEmpty } from "lodash";

/**
 * component representing a collection of books
 * @param books
 * @return {JSX.Element}
 * @constructor
 */
const BooksListScreen = ({books}: bookListProps) => {
	const {search} = useContext(ArticleContext);
	const {filter} = useContext(FilterContext);
	
	/**
	 * filter books
	 * @param {containBook[]}books
	 * @return {containBook[]|Array<T>}
	 */
	const filterBooks = (books: containBook[]) => {
		if  (isEmpty(filter)) return books;
		
		return books.sort((a, b) => {
			if ((filter === "croissant") && (parseInt(a.price) < parseInt(b.price))) return -1;
			if ((filter === "décroissant") && (parseInt(a.price) > parseInt(b.price))) return 1;
			return 0;
		});
	};
	
	/**
	 * research management
	 * @param tab
	 * @return {Array<$NonMaybeType<T>>|Array<T>|Array<*>}
	 */
	const displaysBooksAccordingResearchStatus = (tab: Array<*>): Array<*> => {
		if (isEmpty(search)) return tab;
		return tab.filter(item => item.title.toUpperCase().indexOf(search.toUpperCase()) !== -1);
	};
	
	return (
		<div className="books-list">
			{
				displaysBooksAccordingResearchStatus(filterBooks(books))
					.map((({isbn, cover, synopsis, title, price}, i) => (
						<Book
							key={isbn} cover={cover} synopsis={synopsis}
							title={title} isbn={isbn} price={price} id={`book-spec-${i}`}
						/>
					)))
			}
		</div>
	);
};

export default withRouter(BooksListScreen);
