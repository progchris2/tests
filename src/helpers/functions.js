//@flow
import form from "../conf/models/form.json";
import header from "../conf/models/header.json";
import { isEmpty, isArray } from "lodash";

/**
 *
 * @param {Array}tab
 * @param {number}limit
 * @param {boolean}choice
 * @return {*|string[]}
 */
export const normalizerText = (tab: any[], limit:number =0, choice: boolean = false): any[]|string[] => {
	if (typeof tab[0] !== "string" && !choice) {
		throw new Error("is not a character chain");
	}
	return choice ? tab : Array(tab[0].substr(0, limit) + " ...");
};

/**
 *
 * @param {string}n
 * @return {any}
 */
export const space = (n: number): any => {
	if (typeof n === "string") return;
	return 1/n * 100;
};

/**
 *
 * @param {string}st
 * @param {string}choice
 * @return any
 */
export const getModelElementDOM = (st: string, choice: string = "form"): any => {
	let data = choice === "form" ? form : header;
	return data[st];
};

/**
 *
 * @param tab
 * @return {number}
 */
export const displaysNumberOfItems = (tab: any[]) => {
	if (isArray(tab)) {
		if (isEmpty(tab)) return 0;
		let counter = 0;
		
		tab.forEach(item => {
			if (typeof item === "string" || typeof item === "string") return tab.length;
			counter += parseInt(item.count, 10);
		});
		return counter;
	}
	return 0;
};

/**
 *merge objects with the same identifier
 * @param tab
 * @return {[]}
 */
export const normalizerDataArticle = (tab: Array<*>): Array<*> => {
	return tab.reduce((acc, curr) =>{
		const foundObject = acc.findIndex(item => item.isbn === curr.isbn);
		if(foundObject === -1){
			acc.push(curr.count);
		}else{
			acc[foundObject].count = acc[foundObject].count + curr.count;
		}
		return acc;
	},[]);
	
};

/**
 * normalize the url of the API
 * @param params
 * @return {string}
 */
export const getBaseUrl = (params: Array<*>): string => {
	let t = "";
	params.forEach(item => {
		if  (item.count === 1) {
			t = item.book.isbn;
		}
		if (item.count > 1) {
			for (let i = 0; i < item.count; i++) {
				t += i  === 0 ? item.book.isbn : "," + item.book.isbn;
			}
		}
	});
	return t;
};

/**
 *
 * @param tab
 * @param price
 * @return {number}
 */
export const getBestPromotion = (tab: Object<any>, price: number): mixed => {
	let tabReduction: Array<*> = [];
	if (isEmpty(tab)) return tabReduction;
	
	tab["offers"].forEach(item => {
		if (item.type === "percentage")
			tabReduction.push(price - (item.value * price/100));
		
		if (item.type === "minus")
			tabReduction.push(price - item.value);
		
		if (item.type === "slice") {
			let n = 1;
			let value = 0;
			let condition = price;
			
			do {
				value = price - n * item.value;
				n += 1;
				condition -= item.sliceValue;
			} while (condition >= 100);
			tabReduction.push(value);
		}
	});
	return Math.min(...tabReduction);
};
