import { describe, it } from "@jest/globals";
import {
	normalizerText, space, getModelElementDOM, normalizerDataArticle,
	displaysNumberOfItems, getBestPromotion
} from "./functions";

describe("functions des assistants de test", () => {
	it("check the behavior of the function if one of the parameters is true", () => {
		let resp = normalizerText([1, 2, 3], 0, true);
		expect(resp).toEqual([1, 2, 3]);
	});
	
	it("check the behavior of the function the array contains string and that the second parameter is false", () => {
		let resp = normalizerText(["gilles", "progchris", "commee"], 3, false);
		expect(resp).toEqual(["gil ..."]);
	});
});

describe("functions helpers space", () => {
	it("returns the expected number", () => {
		let resp = space(2);
		expect(resp).toEqual(50);
	});
	
	it("envoie d'un nombre en chaine de caractère", () => {
		let resp = space("toto");
		expect(resp).toEqual(undefined);
	});
});

describe("functions helpers getModelElementDOM", () => {
	it("get the model corresponds to a json key(search)", () => {
		let resp = getModelElementDOM("search");
		expect(resp).toEqual( [{
			"type": "text",
			"name": "search"
		}]);
	});
	
	it("get the model corresponds to a json key (choice-article)", () => {
		let resp = getModelElementDOM("choice-article");
		expect(resp).toEqual([{
			"type": "number",
			"name": "count"
		},
		{
			"type": "submit"
		}]);
	});
	
	it("get the model corresponds to a json key (logo)", () => {
		let resp = getModelElementDOM("logo",  "header");
		expect(resp).toEqual({
			"icon": ""
		});
	});
});

describe("functions helpers normalizerDataArticle", () => {
	
	it.skip("factorize an array if the ids are identical with two object different", () => {
		let resp = normalizerDataArticle([{count: 2, isbn: 0}, {count: 1, isbn: 1}]);
		expect(resp).toEqual([{count: 2, isbn: 0}, {count: 1, isbn: 1}]);
	});
	
	it.skip("factorize an array if the ids are identical with three object different", () => {
		let resp = normalizerDataArticle([{count: 2, isbn: 0}, {count: 1, isbn: 1}, {count: 1, isbn: 2}]);
		expect(resp).toEqual([{count: 2, isbn: 0}, {count: 1, isbn: 1}, {count: 1, isbn: 2}]);
	});
	
	it.skip("factorize an array if the ids are identical with 4 object different", () => {
		let resp = normalizerDataArticle([{count: 2, isbn: 0}, {count: 1, isbn: 1}, {count: 1, isbn: 2}, {count: 1, isbn: 3}]);
		expect(resp).toEqual([{count: 2, isbn: 0}, {count: 1, isbn: 1}, {count: 1, isbn: 2}, {count: 1, isbn: 3}]);
	});
	
	it.skip("factorize an array if the ids are identical with two object uniform", () => {
		let resp = normalizerDataArticle([{count: 2, isbn: 0}, {count: 1, isbn: 0}]);
		expect(resp).toStrictEqual([{count: 3, isbn: 0}]);
	});
	
	it.skip("factorize an array if the ids are identical with three object uniform", () => {
		let resp = normalizerDataArticle([{count: 2, isbn: 0}, {count: 1, isbn: 0}, {count: 9, isbn: 0}]);
		expect(resp).toStrictEqual([{count: 12, isbn: 0}]);
	});
});

describe("functions helpers getBestPromotion", () => {
	it("get the most attractive promotion", () => {
		let resp = getBestPromotion({"offers": [{ "type": "percentage", "value": 5 },
			{ "type": "minus", "value": 15 },
			{ "type": "slice", "sliceValue": 100, "value": 12 }]}, 65);
		expect(resp).toEqual(50);
	});
	it("get the most attractive promotion percentage", () => {
		let resp = getBestPromotion({"offers": [{ "type": "percentage", "value": 5 }]}, 65);
		expect(resp).toEqual(61.75);
	});
});

/*

*/