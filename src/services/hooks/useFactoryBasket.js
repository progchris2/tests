import { useContext, useState } from "react";
import { ArticleContext } from "../context";

const useFactoryBasket = () => {
	const [collectArticle, setCollectArticle] = useState({});
	const {article, promotion, handleDispatch} = useContext(ArticleContext);
	
	return{
		collectArticle,
		promotion,
		handleDispatch,
		verifyLocalStorage: () => {
			// eslint-disable-next-line no-undef
			const basket = localStorage.getItem("@data-articles");
			if (basket) {
				setCollectArticle(JSON.parse(basket));
			} else {
				setCollectArticle(article);
			}
		}
	};
};

export default useFactoryBasket;
