import { useCallback, useReducer } from "react";
import reducerBooks from "../stores/book.reducer";
import ApiFetch, { ApiError } from "../api-fetch/ApiFetch";
import * as types from "../stores/types";
import { initBook } from "../../constantes";

/**
 *
 * @return {{
 * getAllBooks: (...args: any[]) => any,
 * books: React.ReducerStateWithoutAction<function(*=, {type: *, payload?: *}): *>,
 * getABookPromotion: (...args: any[]) => any}}
 */
const useBookshop = () => {
	const [books, dispatch] = useReducer(reducerBooks, initBook);
	return {
		books,
		dispatch,
		getAllBooks: () => {
			ApiFetch("books")
				.then(resp => {
					localStorage.setItem("@data-books", JSON.stringify(resp));
					dispatch({type: types.BOOK.IS_SUCCESS, payload: resp});
				})
				.catch(err => {
					if (err instanceof ApiError) dispatch({type: types.BOOK.IS_FAIL, payload: err});
					else dispatch({type: types.BOOK.IS_FAIL, payload: err});
				});
		}
	};
};

export default useBookshop;
