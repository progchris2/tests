import { useState } from "react";

/**
 * hook managing the state of a book and its modal
 * @return {{setBook: React.Dispatch<React.SetStateAction<{}>>, setModal: React.Dispatch<React.SetStateAction<boolean>>, book: {}, modal: boolean}}
 */
const useBook = () => {
	const [book, setBook] = useState({});
	const [modal, setModal] = useState(false);
	
	return {
		book,
		modal,
		setBook,
		setModal
	};
};

export default useBook;
