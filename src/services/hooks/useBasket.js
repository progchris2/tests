import { useReducer, useState } from "react";

import { initArticle } from "../../constantes";
import reducerBasket from "../stores/basket.reducer";
import { getBaseUrl } from "../../helpers/functions";
import ApiFetch from "../api-fetch/ApiFetch";

/**
 *
 * @return {{dispatch: *, article: *}}
 */
const useBasket = () => {
	const [search, setSearch] = useState("");
	const [promotion, setPromotion] = useState({});
	const [article, dispatch] = useReducer(reducerBasket, initArticle);
	
	return {
		search,
		article,
		setSearch,
		promotion,
		dispatch,
		getPromotionArticle: (params) => {
			let url = `books/${getBaseUrl(params)}/commercialOffers`;
			ApiFetch(url)
				.then(resp => setPromotion(resp))
				.catch(err => console.log(err));
		}
	};
};

export default useBasket;

