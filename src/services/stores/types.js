/**
 *
 * @type {{IS_SELECTED: string, IS_FAIL: string, IS_SUCCESS: string}}
 */
export const BOOK = {
	IS_SUCCESS : "IS_SUCCESS",
	IS_SELECTED: "IS_SELECTED",
	IS_FAIL    : "IS_FAIL"
};

/**
 *
 * @type {{DELETE_ARTICLE: string, ADD_ARTICLE: string}}
 */
export const BASKET = {
	ADD_ARTICLE             : "ADD_ARTICLE",
	DELETE_ALL_ARTICLE      : "DELETE_ALL_ARTICLE",
	DELETE_ONE_COUNT_ARTICLE: "DELETE_ONE_COUNT_ARTICLE",
	ADD_ONE_COUNT_ARTICLE   : "ADD_ONE_COUNT_ARTICLE"
};