import * as types from "./types";
import { createReducer } from "../../conf/utils/reducers.utils";
import { initBook } from "../../constantes";


/**
 * get the API books
 * @param state
 * @param payload
 * @returns {Object}
 */
export const isBooksSuccess = (state: string, payload: Array) => {
	const bks = {...state};
	bks.books = payload;
	return bks;
};

/**
 * handles related errors returned by the API
 * @param state
 * @param payload
 * @returns {Object}
 */
export const errorBookFail = (state: string, payload: any) => {
	const err = {...state};
	err.errorMsg = payload;
	return err;
};


export default createReducer(initBook, {
	[types.BOOK.IS_SUCCESS]: isBooksSuccess,
	[types.BOOK.IS_FAIL]   : errorBookFail
});
