import { createReducer } from "../../conf/utils/reducers.utils";
import { initArticle } from "../../constantes";
import * as types from "./types";

/**
 *
 * @param state
 * @param payload
 * @return {{[p: number]: *, some(callbackfn: (value: *, index: number, array: *[]) => unknown, thisArg?: any): boolean, keys(): IterableIterator<number>, shift(): (* | undefined), values(): IterableIterator<*>, pop(): (* | undefined), slice(start?: number, end?: number): *[], find: {<S extends *>(predicate: (this:void, value: *, index: number, obj: *[]) => value is S, thisArg?: any): (S | undefined), (predicate: (value: *, index: number, obj: *[]) => unknown, thisArg?: any): (* | undefined)}, flat: {<U>(this:U[][][][][][][][], depth: 7): U[], <U>(this:U[][][][][][][], depth: 6): U[], <U>(this:U[][][][][][], depth: 5): U[], <U>(this:U[][][][][], depth: 4): U[], <U>(this:U[][][][], depth: 3): U[], <U>(this:U[][][], depth: 2): U[], <U>(this:U[][], depth?: 1): U[], <U>(this:U[], depth: 0): U[], <U>(depth?: number): any[], <A, D=1 extends number>(this:A, depth?: D): FlatArray<A, D>[]}, join(separator?: string): string, reduceRight: {(callbackfn: (previousValue: *, currentValue: *, currentIndex: number, array: *[]) => *): *, (callbackfn: (previousValue: *, currentValue: *, currentIndex: number, array: *[]) => *, initialValue: *): *, <U>(callbackfn: (previousValue: U, currentValue: *, currentIndex: number, array: *[]) => U, initialValue: U): U}, copyWithin(target: number, start: number, end?: number): this, indexOf(searchElement: *, fromIndex?: number): number, every(callbackfn: (value: *, index: number, array: *[]) => unknown, thisArg?: any): boolean, map<U>(callbackfn: (value: *, index: number, array: *[]) => U, thisArg?: any): U[], reduce: {(callbackfn: (previousValue: *, currentValue: *, currentIndex: number, array: *[]) => *): *, (callbackfn: (previousValue: *, currentValue: *, currentIndex: number, array: *[]) => *, initialValue: *): *, <U>(callbackfn: (previousValue: U, currentValue: *, currentIndex: number, array: *[]) => U, initialValue: U): U}, splice: {(start: number, deleteCount?: number): *[], (start: number, deleteCount: number, ...items: *[]): *[]}, forEach(callbackfn: (value: *, index: number, array: *[]) => void, thisArg?: any): void, [Symbol.iterator](): IterableIterator<*>, length: number, includes(searchElement: *, fromIndex?: number): boolean, concat: {(...items: ConcatArray<*>): *[], (...items: ConcatArray<*> | *[]): *[]}, sort(compareFn?: (a: *, b: *) => number): this, reverse(): *[], fill(value: *, start?: number, end?: number): this, push(...items: *[]): number, [Symbol.unscopables](): {copyWithin: boolean, entries: boolean, fill: boolean, find: boolean, findIndex: boolean, keys: boolean, values: boolean}, filter: {<S extends *>(callbackfn: (value: *, index: number, array: *[]) => value is S, thisArg?: any): S[], (callbackfn: (value: *, index: number, array: *[]) => unknown, thisArg?: any): *[]}, findIndex(predicate: (value: *, index: number, obj: *[]) => unknown, thisArg?: any): number, flatMap: {<U, This=undefined>(callback: (this:This, value: *, index: number, array: *[]) => (ReadonlyArray<U> | U), thisArg?: This): U[], <U, This=undefined>(callback: (this:This, value: *, index: number, array: *[]) => (ReadonlyArray<U> | U), thisArg?: This): U[]}, lastIndexOf(searchElement: *, fromIndex?: number): number, entries(): IterableIterator<[number, *]>, toString(): string, unshift(...items: *[]): number, toLocaleString(): string}}
 */
export const addArticle = (state: Array<*>, payload: Array<*>) => {
	const art = {...state};
	const index = art.article.findIndex(item => item.book.isbn === payload.book.isbn);
	switch (-1) {
		case index:
			art.article.push(payload);
			break;
		default:
			art.article[index].count += payload.count;
			break;
	}
	// eslint-disable-next-line no-undef
	localStorage.setItem("@data-articles", JSON.stringify(art));
	return art;
};

/**
 *  get the API books
 * @param state
 * @param payload
 * @return {{[p: number]: *, some(callbackfn: (value: *, index: number, array: *[]) => unknown, thisArg?: any): boolean, keys(): IterableIterator<number>, shift(): (* | undefined), values(): IterableIterator<*>, pop(): (* | undefined), slice(start?: number, end?: number): *[], find: {<S extends *>(predicate: (this:void, value: *, index: number, obj: *[]) => value is S, thisArg?: any): (S | undefined), (predicate: (value: *, index: number, obj: *[]) => unknown, thisArg?: any): (* | undefined)}, flat: {<U>(this:U[][][][][][][][], depth: 7): U[], <U>(this:U[][][][][][][], depth: 6): U[], <U>(this:U[][][][][][], depth: 5): U[], <U>(this:U[][][][][], depth: 4): U[], <U>(this:U[][][][], depth: 3): U[], <U>(this:U[][][], depth: 2): U[], <U>(this:U[][], depth?: 1): U[], <U>(this:U[], depth: 0): U[], <U>(depth?: number): any[], <A, D=1 extends number>(this:A, depth?: D): FlatArray<A, D>[]}, join(separator?: string): string, reduceRight: {(callbackfn: (previousValue: *, currentValue: *, currentIndex: number, array: *[]) => *): *, (callbackfn: (previousValue: *, currentValue: *, currentIndex: number, array: *[]) => *, initialValue: *): *, <U>(callbackfn: (previousValue: U, currentValue: *, currentIndex: number, array: *[]) => U, initialValue: U): U}, copyWithin(target: number, start: number, end?: number): this, indexOf(searchElement: *, fromIndex?: number): number, every(callbackfn: (value: *, index: number, array: *[]) => unknown, thisArg?: any): boolean, map<U>(callbackfn: (value: *, index: number, array: *[]) => U, thisArg?: any): U[], reduce: {(callbackfn: (previousValue: *, currentValue: *, currentIndex: number, array: *[]) => *): *, (callbackfn: (previousValue: *, currentValue: *, currentIndex: number, array: *[]) => *, initialValue: *): *, <U>(callbackfn: (previousValue: U, currentValue: *, currentIndex: number, array: *[]) => U, initialValue: U): U}, splice: {(start: number, deleteCount?: number): *[], (start: number, deleteCount: number, ...items: *[]): *[]}, forEach(callbackfn: (value: *, index: number, array: *[]) => void, thisArg?: any): void, [Symbol.iterator](): IterableIterator<*>, length: number, includes(searchElement: *, fromIndex?: number): boolean, concat: {(...items: ConcatArray<*>): *[], (...items: ConcatArray<*> | *[]): *[]}, sort(compareFn?: (a: *, b: *) => number): this, reverse(): *[], fill(value: *, start?: number, end?: number): this, push(...items: *[]): number, [Symbol.unscopables](): {copyWithin: boolean, entries: boolean, fill: boolean, find: boolean, findIndex: boolean, keys: boolean, values: boolean}, filter: {<S extends *>(callbackfn: (value: *, index: number, array: *[]) => value is S, thisArg?: any): S[], (callbackfn: (value: *, index: number, array: *[]) => unknown, thisArg?: any): *[]}, findIndex(predicate: (value: *, index: number, obj: *[]) => unknown, thisArg?: any): number, flatMap: {<U, This=undefined>(callback: (this:This, value: *, index: number, array: *[]) => (ReadonlyArray<U> | U), thisArg?: This): U[], <U, This=undefined>(callback: (this:This, value: *, index: number, array: *[]) => (ReadonlyArray<U> | U), thisArg?: This): U[]}, lastIndexOf(searchElement: *, fromIndex?: number): number, entries(): IterableIterator<[number, *]>, toString(): string, unshift(...items: *[]): number, toLocaleString(): string}}
 */
export const addOneCountArticle = (state: Array<*>, payload: Object<*>) => {
	const add = {...state};
	add.article =  add.article.map(item => {
		if (item.book.isbn === payload.book.isbn) item.count += 1;
		return item;
	});
	// eslint-disable-next-line no-undef
	localStorage.setItem("@data-articles", JSON.stringify(add));
	return add;
};

/**
 *
 * @param state
 * @param payload
 * @return {{[p: number]: *, some(callbackfn: (value: *, index: number, array: *[]) => unknown, thisArg?: any): boolean, keys(): IterableIterator<number>, shift(): (* | undefined), values(): IterableIterator<*>, pop(): (* | undefined), slice(start?: number, end?: number): *[], find: {<S extends *>(predicate: (this:void, value: *, index: number, obj: *[]) => value is S, thisArg?: any): (S | undefined), (predicate: (value: *, index: number, obj: *[]) => unknown, thisArg?: any): (* | undefined)}, flat: {<U>(this:U[][][][][][][][], depth: 7): U[], <U>(this:U[][][][][][][], depth: 6): U[], <U>(this:U[][][][][][], depth: 5): U[], <U>(this:U[][][][][], depth: 4): U[], <U>(this:U[][][][], depth: 3): U[], <U>(this:U[][][], depth: 2): U[], <U>(this:U[][], depth?: 1): U[], <U>(this:U[], depth: 0): U[], <U>(depth?: number): any[], <A, D=1 extends number>(this:A, depth?: D): FlatArray<A, D>[]}, join(separator?: string): string, reduceRight: {(callbackfn: (previousValue: *, currentValue: *, currentIndex: number, array: *[]) => *): *, (callbackfn: (previousValue: *, currentValue: *, currentIndex: number, array: *[]) => *, initialValue: *): *, <U>(callbackfn: (previousValue: U, currentValue: *, currentIndex: number, array: *[]) => U, initialValue: U): U}, copyWithin(target: number, start: number, end?: number): this, indexOf(searchElement: *, fromIndex?: number): number, every(callbackfn: (value: *, index: number, array: *[]) => unknown, thisArg?: any): boolean, map<U>(callbackfn: (value: *, index: number, array: *[]) => U, thisArg?: any): U[], reduce: {(callbackfn: (previousValue: *, currentValue: *, currentIndex: number, array: *[]) => *): *, (callbackfn: (previousValue: *, currentValue: *, currentIndex: number, array: *[]) => *, initialValue: *): *, <U>(callbackfn: (previousValue: U, currentValue: *, currentIndex: number, array: *[]) => U, initialValue: U): U}, splice: {(start: number, deleteCount?: number): *[], (start: number, deleteCount: number, ...items: *[]): *[]}, forEach(callbackfn: (value: *, index: number, array: *[]) => void, thisArg?: any): void, [Symbol.iterator](): IterableIterator<*>, length: number, includes(searchElement: *, fromIndex?: number): boolean, concat: {(...items: ConcatArray<*>): *[], (...items: ConcatArray<*> | *[]): *[]}, sort(compareFn?: (a: *, b: *) => number): this, reverse(): *[], fill(value: *, start?: number, end?: number): this, push(...items: *[]): number, [Symbol.unscopables](): {copyWithin: boolean, entries: boolean, fill: boolean, find: boolean, findIndex: boolean, keys: boolean, values: boolean}, filter: {<S extends *>(callbackfn: (value: *, index: number, array: *[]) => value is S, thisArg?: any): S[], (callbackfn: (value: *, index: number, array: *[]) => unknown, thisArg?: any): *[]}, findIndex(predicate: (value: *, index: number, obj: *[]) => unknown, thisArg?: any): number, flatMap: {<U, This=undefined>(callback: (this:This, value: *, index: number, array: *[]) => (ReadonlyArray<U> | U), thisArg?: This): U[], <U, This=undefined>(callback: (this:This, value: *, index: number, array: *[]) => (ReadonlyArray<U> | U), thisArg?: This): U[]}, lastIndexOf(searchElement: *, fromIndex?: number): number, entries(): IterableIterator<[number, *]>, toString(): string, unshift(...items: *[]): number, toLocaleString(): string}}
 */
export const deleteOneCountArticle = (state: Array<*>, payload: Object<*>) => {
	const del = {...state};
	del.article =  del.article.map(item => {
		if (item.book.isbn === payload.book.isbn) {
			if (item.count <= 0) item.count = 0;
			else item.count -= 1;
		}
		return item;
	});
	// eslint-disable-next-line no-undef
	localStorage.setItem("@data-articles", JSON.stringify(del));
	return del;
};

/**
 * handles related errors returned by the API
 * @param state
 * @param payload
 * @return {{[p: number]: string, small(): string, big(): string, codePointAt(pos: number): (number | undefined), replace: {(searchValue: (string | RegExp), replaceValue: string): string, (searchValue: (string | RegExp), replacer: (substring: string, ...args: any[]) => string): string, (searchValue: {[Symbol.replace](string: string, replaceValue: string): string}, replaceValue: string): string, (searchValue: {[Symbol.replace](string: string, replacer: (substring: string, ...args: any[]) => string): string}, replacer: (substring: string, ...args: any[]) => string): string}, fontsize: {(size: number): string, (size: string): string}, localeCompare: {(that: string): number, (that: string, locales?: (string | string[]), options?: Intl.CollatorOptions): number}, trimEnd(): string, toLocaleUpperCase(locales?: (string | string[])): string, split: {(separator: (string | RegExp), limit?: number): string[], (splitter: {[Symbol.split](string: string, limit?: number): string[]}, limit?: number): string[]}, trim(): string, slice(start?: number, end?: number): string, trimLeft(): string, normalize: {(form: ("NFC" | "NFD" | "NFKC" | "NFKD")): string, (form?: string): string}, toUpperCase(): string, fontcolor(color: string): string, indexOf(searchString: string, position?: number): number, trimStart(): string, padStart(maxLength: number, fillString?: string): string, toLowerCase(): string, italics(): string, includes(searchString: string, position?: number): boolean, concat(...strings: string[]): string, bold(): string, substr(from: number, length?: number): string, matchAll(regexp: RegExp): IterableIterator<RegExpMatchArray>, toLocaleLowerCase(locales?: (string | string[])): string, endsWith(searchString: string, endPosition?: number): boolean, fixed(): string, sub(): string, link(url: string): string, blink(): string, replaceAll: {(searchValue: (string | RegExp), replaceValue: string): string, (searchValue: (string | RegExp), replacer: (substring: string, ...args: any[]) => string): string}, substring(start: number, end?: number): string, sup(): string, search: {(regexp: (string | RegExp)): number, (searcher: {[Symbol.search](string: string): number}): number}, repeat(count: number): string, charCodeAt(index: number): number, valueOf(): string, [Symbol.iterator](): IterableIterator<string>, strike(): string, match: {(regexp: (string | RegExp)): (RegExpMatchArray | null), (matcher: {[Symbol.match](string: string): (RegExpMatchArray | null)}): (RegExpMatchArray | null)}, readonly length: number, padEnd(maxLength: number, fillString?: string): string, lastIndexOf(searchString: string, position?: number): number, trimRight(): string, anchor(name: string): string, toString(): string, charAt(pos: number): string, startsWith(searchString: string, position?: number): boolean}}
 */
export const deleteAllArticle = (state: string, payload: string) => {
	const  del = {...state};
	del.article = del.article.filter(item => item.book.isbn !== payload);
	// eslint-disable-next-line no-undef
	localStorage.setItem("@data-articles", JSON.stringify(del));
	return del;
};


export default createReducer(initArticle, {
	[types.BASKET.DELETE_ALL_ARTICLE]      : deleteAllArticle,
	[types.BASKET.ADD_ARTICLE]             : addArticle,
	[types.BASKET.ADD_ONE_COUNT_ARTICLE]   : addOneCountArticle,
	[types.BASKET.DELETE_ONE_COUNT_ARTICLE]: deleteOneCountArticle
});
