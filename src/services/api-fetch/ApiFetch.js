export class ApiError {
	constructor(error) {
		this.error = error;
	}
}

const ApiFetch = async function(path){
	const myHeaders = new Headers({
		"Accept"      : "application/json",
		"Content-Type": "application/json"
	});
	const options = {
		method : "GET",
		headers: myHeaders,
		mode   : "cors",
		cache  : "default"
	};
	const url = "http://henri-potier.xebia.fr/" + path;
	const response = await fetch(url, options);
	
	if (response.ok) {
		return response.json();
	} else {
		throw ApiError(response);
	}
};

export default ApiFetch;
