import {createContext} from "react";

const init = {
	basket: {
		count: 1,
		id   : ""
	},
	handleDispatch: () => {}
};

const initUpdate = {
	updateBasket      : false,
	handleUpdateBasket: () => {}
};

const initFilter = {
	filter      : "",
	handleFilter: () => {}
};

/**
 * context global
 * @type {React.Context<{basket: {count: number, id: string}, handleDispatch: handleDispatch}>}
 */
export const ArticleContext =  createContext(init);

/**
 *
 * @type {React.Context<{handleUpdate: handleUpdate, state: boolean}>}
 */
export const UpdateBasketContext =  createContext(initUpdate);

/**
 *
 * @type {React.Context<{filter: string, handleFilter: handleFilter}>}
 */
export const FilterContext = createContext(initFilter);