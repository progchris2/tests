type containArticle =  {
	count?: string,
	id?: string
}

export type containBook = {
	cover: string,
	title: string,
	price: number
}

export type bookProps = {
	isbn: string,
	title: string,
	price: string,
	cover: string,
	id: string;
	synopsis: string[]
}

export type bookListProps = {
	books: bookProps[]
}

export type wrapperProps = {
	ht?: string,
	wh?: string
}

export type initBookProps  = {
	books        : Array,
	cardDirection: boolean,
	errorMsg: string
}

export type initArticleProps  = {
	article: containArticle[]
}

export type BookScreenProps = {
	books: Array<*>,
	match: Object<*>
};

export type ModalProps = {
	articles: containBook;
	handleOnClick: boolean => boolean;
}