//@flow
import type { initArticleProps, initBookProps } from "../services/types";

/**
 *
 * @type {{books: [], cardDirection: boolean, errorMsg: string}}
 */
export const initBook: initBookProps = {
	books        : [],
	cardDirection: false,
	errorMsg     : ""
};

/**
 *
 * @type {{article: []}}
 */
export const initArticle: initArticleProps = {
	article: []
};