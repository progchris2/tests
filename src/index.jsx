import React from "react";
import {render} from "react-dom";
import Routes from "./conf/routes/routes";

render(
	<Routes />,
	/* eslint-disable-next-line no-undef */
	document.getElementById("app-root")
);
