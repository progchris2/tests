//@flow
import React, { useContext } from "react";

import {HeaderApp} from "../index";
import { getModelElementDOM } from "../../helpers/functions";
import Form from "../form/form";
import FilterBooks from "../filter/filter";
import { FilterContext } from "../../services/context";

type LayoutAppProps = {
	children: string;
}

/**
 * web application template
 * @param children
 * @return {JSX.Element}
 * @constructor
 */
const LayoutApp = ({children}: LayoutAppProps) => {
	const {handleFilter} = useContext(FilterContext);
	
	return (
		<div id="layout">
			<div className="header"><HeaderApp/></div>
			<div>
				<div className="section">
					<div className="widget">
						<div className="separator">
							<h4>Recherches</h4>
							<div className="form">
								<Form data={getModelElementDOM("search")}/>
							</div>
						</div>
						<div className="separator">
							<h4>Filtres</h4>
							<FilterBooks handleFilter={handleFilter}/>
						</div>
						<div className="separator">
							<h4>Thèmes</h4>
						</div>
					</div>
					<div className="aside">
						{children}
					</div>
				</div>
			</div>
			<div className="footer">
				footer
			</div>
		</div>
	);
};

export default LayoutApp;
