//@flow
import React, { useContext } from "react";
import { Link } from "react-router-dom";

import { Wrapper } from "../../ui";
import { BasketIcon } from "../../ui/icons";
import dataHeader from "../../conf/models/header.json";
import BadgeBasket from "../badge-basket/badge-basket";
import { ArticleContext } from "../../services/context";
import logo  from "../../static/images/lire-logo-du-livre.jpg";


const HeaderApp = () => {
	const {handleSearchBook} = useContext(ArticleContext);
	
	return (
		<Wrapper wh={1}>
			<div id="banner">
				<Link to="/" onClick={() => handleSearchBook("")}>
					<div className="logo">
						<img src={logo} alt="" style={{width: "45px"}}/>
					</div>
				</Link>
				<div className="title">{dataHeader.title.title}</div>
				<div className="basket">
					<Link to="/basket">
						<BasketIcon color="currentColor" />
						<BadgeBasket />
					</Link>
				</div>
			</div>
		</Wrapper>
	);
};

export default HeaderApp;
