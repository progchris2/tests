//@flow
import React, { useContext, useEffect } from "react";

import { Badge } from "../../ui";
import { displaysNumberOfItems } from "../../helpers/functions";
import useFactoryBasket from "../../services/hooks/useFactoryBasket";
import { UpdateBasketContext } from "../../services/context";

/**
 * Badge
 * @return {JSX.Element}
 * @constructor
 */
const BadgeBasket = () => {
	const {updateBasket} = useContext(UpdateBasketContext);
	const {collectArticle, verifyLocalStorage} = useFactoryBasket();
	
	useEffect(() => {
		verifyLocalStorage();
	}, [updateBasket]);
	
	return (
		<Badge>
			{collectArticle.article && displaysNumberOfItems(collectArticle.article)}
		</Badge>
	);
};

export default BadgeBasket;
