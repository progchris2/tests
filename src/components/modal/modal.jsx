//@flow
import React from "react";
import { createPortal } from "react-dom";
import { ButtonCustom } from "../../ui";
import { Link } from "react-router-dom";
import type { ModalProps } from "../../services/types";


/**
 *
 * @param articles
 * @param handleOnClick
 * @return {JSX.Element}
 * @constructor
 */
const Modal = ({handleOnClick, articles}: ModalProps) => {
	return (
		createPortal(
			<div  className="wrap-modal">
				<div className="modal">
					{
						articles && articles.map(({book, count}, i) => <div key={`items-${i}`}>
							<div className="head-modal">
								<img src={book.cover} style={{width: "100px", height: "150px"}} alt=""/>
								<div>
									<div>{book.title}</div>
									<div>{book.price * count} &euro;</div>
								</div>
							</div>
							<hr/>
						</div>)
					}
					<div className="footer-modal">
						<ButtonCustom data-cy="panier"><Link to="/basket">Voir panier</Link></ButtonCustom>
						<ButtonCustom data-cy="achat" onClick={handleOnClick}>Continuer les achat</ButtonCustom>
					</div>
				</div>
			</div>,
			// eslint-disable-next-line no-undef
			document.getElementById("modal-root")
		)
	);
};

export default Modal;



