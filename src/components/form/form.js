import React, { useContext, useState } from "react";
import Field from "../../ui/field";
import { ArticleContext, UpdateBasketContext } from "../../services/context";
import * as types from "../../services/stores/types";

type FormProps = {
	data: Array<*>,
	book?: Object,
	handleModal?: (boolean) => boolean
}

/**
 * generic form
 * @param data
 * @param book
 * @param handleModal
 * @return {JSX.Element}
 * @constructor
 */
const Form = ({data, book, handleModal}: FormProps) => {
	const [count, setCount] = useState(1);
	const {handleDispatch, search, handleSearchBook} = useContext(ArticleContext);
	const {handleUpdateBasket} = useContext(UpdateBasketContext);
	
	const handleSubmit = (e) => {
		e.preventDefault();
		const payload = {
			count,
			book,
			modal: false
		};
		handleDispatch({
			type: types.BASKET.ADD_ARTICLE,
			payload
		});
		handleUpdateBasket(true);
		handleModal(true);
	};
	
	// eslint-disable-next-line no-undef
	const handleChange = (e: SyntheticInputEvent<HTMLInputElement>) => setCount(parseInt(e.target.value, 10));
	
	// eslint-disable-next-line no-undef
	const handleSearch = (e: SyntheticInputEvent<HTMLInputElement>) => handleSearchBook(e.target.value);
	
	return (
		<form onSubmit={handleSubmit}>
			{
				data.map(({type, name}, i) => (
					<Field
						name={name} type={type} key={`choice-${i}`}
						value={(type === "number" && count) || (type === "text" && search) }
						handleChange={(type === "number" && handleChange) || (type === "text" && handleSearch) }
					/>
				))
			}
		</form>
	);
};

export default Form;
