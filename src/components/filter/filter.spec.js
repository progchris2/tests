import React from "react";
import { describe, it } from "@jest/globals";
import {render} from "@testing-library/react";
import { fireEvent, screen } from "@testing-library/dom";
import FilterBooks from "./filter";

describe("filter component function test", () => {
	it("label display décroissant", () => {
		render(<FilterBooks handleFilter={() => null}/>);
		let label = screen.getByText("décroissant");
		expect(label).toBeInTheDocument();
	});
	
	it("label display croissant", () => {
		render(<FilterBooks handleFilter={() => null} />);
		let label = screen.getByText("croissant");
		expect(label).toBeInTheDocument();
	});
	
	it("check if the click is executed normally", () => {
		const mockChange = jest.fn();
		render(<FilterBooks handleFilter={mockChange} />);
		// eslint-disable-next-line no-undef
		let input = document.getElementById("croissant");
		fireEvent.change(input);
		//expect(mockChange.mock.calls).toContain("croissant");
	});
});