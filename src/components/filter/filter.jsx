import React  from "react";
import { check } from "../../conf/models/checkbox";
import { Input } from "../../ui";

type FilterBooksType = {
	handleFilter: (string) => string
};

/**
 * filter component
 * @param handleFilter
 * @return {JSX.Element}
 * @constructor
 */
const FilterBooks = ({handleFilter}: FilterBooksType) => {
	return (
		<div>
			{
				check.map(({name, type}, i) => (
					<div key={`radio-${i}`} className="filter-books">
						<Input
							type={type} name="filter" data-cy={name} value={name}
							onChange={(e) => handleFilter(e.target.value)} ckecked
						/>
						<label htmlFor={name}>{name}</label>
					</div>
				))
			}
		</div>
	);
};

export default FilterBooks;
