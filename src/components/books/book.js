import React from "react";

import type { bookProps } from "../../services/types";
import { Card } from "../../ui";
import { normalizerText } from "../../helpers/functions";
import { Link } from "react-router-dom";

/**
 * component representing a book
 * @param isbn
 * @param cover
 * @param title
 * @param synopsis
 * @param price
 * @param id
 * @return {JSX.Element}
 * @constructor
 */
const Book = ({cover, title, synopsis, price, isbn, id}: bookProps) => {
	const path = `/books/${isbn}`;

	return (
		<Card direction="column" shadow={true}  data-cy={id}>
			<Link to={path}>
				<div style={{textAlign: "center", marginTop: "20px"}}>
					<img src={cover} alt="" style={{width: "240px", height: "340px"}}/>
				</div>
				<div>
					<Card className="card-body" direction="row">
						<h3>{title}</h3>
						<div>{price}&euro; </div>
					</Card>
					<hr />
				</div>
				<div className="synopsis">
					{normalizerText(synopsis, 190).map((item, i) => <p key={i}>{item}</p>)}
				</div>
			</Link>
		</Card>
	);
};

export default Book;
