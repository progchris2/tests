describe("behavior of all actions", () => {
	beforeEach(() => {
		cy.visit("http://localhost:1234/");
	});
	context("launch execute a filter on the bookScreen page decrease",  () => {
		it("filtre décroissant", () => {
			cy.get("[data-cy=décroissant]").click();
		});
	});
	
	context("launch execute a filter book ascending",  () => {
		it("filtre croissant", () => {
			cy.get("[data-cy=croissant]").click();
		});
	});
	
	context("launch a search on the bookScreen page",  () => {
		it("recherche", () => {
			cy.get("[data-cy=search]").type("sorcier");
			cy.get("[data-cy=book-spec-0]").click();
			cy.get("[data-cy=submit]").click();
			cy.get("[data-cy=panier]").click();
		});
	});
	
	context("launch a search on the bookScreen page incremente",  () => {
		it("incremente", () => {
			cy.get("[data-cy=book-spec-2]").click();
			cy.get("[data-cy=count]").clear();
			cy.get("[data-cy=count]").type("2");
			cy.get("[data-cy=submit]").click();
			cy.get("[data-cy=panier]").click();
		});
	});
	
});